#!/bin/bash

nc -z -w5 localhost $SC_API_PORT
result1=$?

if [ "$result1" == 0 ]; then
  echo 'spd looks to be running'
else
  export SCPRIME_WALLET_PASSWORD=PrimePassword
  echo 'starting spd...'
  ./spd -M cgthrw --host-addr :$SC_HOST_PORT --api-addr localhost:$SC_API_PORT --rpc-addr localhost:$SC_RPC_PORT &
fi

