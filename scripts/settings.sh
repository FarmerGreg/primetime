#!/bin/bash
SC_API_PORT=14280

./spc --addr :$SC_API_PORT host config maxduration 12W

./spc --addr :$SC_API_PORT host config collateral 15KS
./spc --addr :$SC_API_PORT host config collateralbudget 1MS
./spc --addr :$SC_API_PORT host config maxcollateral 60KS

#./hsc host config mincontractprice 5SCP
./spc --addr :$SC_API_PORT host config mindownloadbandwidthprice 200SCP
./spc --addr :$SC_API_PORT host config minstorageprice 10KS
./spc --addr :$SC_API_PORT host config minuploadbandwidthprice 200SCP

